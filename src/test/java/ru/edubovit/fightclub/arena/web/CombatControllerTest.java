package ru.edubovit.fightclub.arena.web;

import ru.edubovit.fightclub.arena.dao.CombatDAO;
import ru.edubovit.fightclub.arena.domain.Action;
import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.domain.Fighter;
import ru.edubovit.fightclub.arena.domain.enums.Target;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Set;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CombatControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private CombatDAO combatDAO;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Test
  void create_sanity() throws Exception {
    mvc.perform(post("/combat"))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", greaterThan(0L), Long.class))
        .andDo(print());
  }

  @Test
  void getById_success() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    mvc.perform(get("/combat/" + id))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", equalTo(id), Long.class))
        .andExpect(jsonPath("$.winner", nullValue()))
        .andExpect(jsonPath("$.actions", empty()))
        .andExpect(jsonPath("$.events", empty()))
        .andExpect(jsonPath("$.teamFirst.name", equalTo(combat.getTeamFirst().getName())))
        .andExpect(jsonPath("$.teamFirst.health", equalTo(combat.getTeamFirst().getHealth())))
        .andExpect(jsonPath("$.teamFirst.healthMax", equalTo(combat.getTeamFirst().getHealthMax())))
        .andExpect(jsonPath("$.teamSecond.name", equalTo(combat.getTeamSecond().getName())))
        .andExpect(jsonPath("$.teamSecond.health", equalTo(combat.getTeamSecond().getHealth())))
        .andExpect(jsonPath("$.teamSecond.healthMax", equalTo(combat.getTeamSecond().getHealthMax())))
        .andDo(print());
  }

  @Test
  void getById_notFound() throws Exception {
    mvc.perform(get("/combat/-999"))
        .andExpect(status().isNotFound())
        .andDo(print());
  }

  @Test
  void getById_badId() throws Exception {
    mvc.perform(get("/combat/dummy"))
        .andExpect(status().isBadRequest())
        .andDo(print());
  }

  @Test
  void postAction_success() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var action = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.HEAD))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(action)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertEquals(1, combat.getActions().size());
    assertEquals(0, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_invalidTargetsCount() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var action = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.HEAD, Target.LEGS))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(action)))
        .andExpect(status().isBadRequest())
        .andDo(print());

    assertEquals(0, combat.getActions().size());
    assertEquals(0, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_duplicate() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var action = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.HEAD))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(action)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(action)))
        .andExpect(status().isConflict())
        .andDo(print());

    assertEquals(1, combat.getActions().size());
    assertEquals(0, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_turn_success() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var actionFirst = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.HEAD))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    var actionSecond = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamSecond().getName())
            .build())
        .attack(Set.of(Target.HEAD))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionFirst)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionSecond)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertTrue(combat.getTeamFirst().getHealth() < combat.getTeamFirst().getHealthMax());
    assertTrue(combat.getTeamSecond().getHealth() < combat.getTeamSecond().getHealthMax());
    assertEquals(2, combat.getActions().size());
    assertEquals(2, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_turn_bothBlocked() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var actionFirst = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.BODY))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    var actionSecond = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamSecond().getName())
            .build())
        .attack(Set.of(Target.BODY))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionFirst)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionSecond)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertEquals(combat.getTeamFirst().getHealth(), combat.getTeamFirst().getHealthMax());
    assertEquals(combat.getTeamSecond().getHealth(), combat.getTeamSecond().getHealthMax());
    assertEquals(2, combat.getActions().size());
    assertEquals(2, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_turn_firstBlocked() throws Exception {
    var combat = createCombat();
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var actionFirst = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.LEGS))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    var actionSecond = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamSecond().getName())
            .build())
        .attack(Set.of(Target.BODY))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionFirst)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionSecond)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertEquals(combat.getTeamFirst().getHealth(), combat.getTeamFirst().getHealthMax());
    assertTrue(combat.getTeamSecond().getHealth() < combat.getTeamSecond().getHealthMax());
    assertEquals(2, combat.getActions().size());
    assertEquals(2, combat.getEvents().size());
    assertNull(combat.getWinner());
  }

  @Test
  void postAction_turn_firstWins() throws Exception {
    var combat = createCombat();
    combat.getTeamSecond().setHealth(1);
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var actionFirst = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.LEGS))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    var actionSecond = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamSecond().getName())
            .build())
        .attack(Set.of(Target.BODY))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionFirst)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionSecond)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertEquals(combat.getTeamFirst().getHealth(), combat.getTeamFirst().getHealthMax());
    assertEquals(0, combat.getTeamSecond().getHealth());
    assertEquals(2, combat.getActions().size());
    assertEquals(3, combat.getEvents().size());
    assertEquals(Combat.Winner.FIRST, combat.getWinner());
  }

  @Test
  void postAction_turn_tie() throws Exception {
    var combat = createCombat();
    combat.getTeamFirst().setHealth(1);
    combat.getTeamSecond().setHealth(1);
    combatDAO.save(combat);
    Long id = combat.getId();
    assertTrue(id > 0);

    var actionFirst = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamFirst().getName())
            .build())
        .attack(Set.of(Target.LEGS))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    var actionSecond = Action.builder()
        .owner(Fighter.builder()
            .name(combat.getTeamSecond().getName())
            .build())
        .attack(Set.of(Target.LEGS))
        .protect(Set.of(Target.LEFT_HAND, Target.BODY, Target.RIGHT_HAND))
        .build();

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionFirst)))
        .andExpect(status().isCreated())
        .andDo(print());

    mvc.perform(post("/combat/" + id + "/action")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(actionSecond)))
        .andExpect(status().isCreated())
        .andDo(print());

    assertEquals(0, combat.getTeamFirst().getHealth());
    assertEquals(0, combat.getTeamSecond().getHealth());
    assertEquals(2, combat.getActions().size());
    assertEquals(3, combat.getEvents().size());
    assertEquals(Combat.Winner.TIE, combat.getWinner());
  }

  private Combat createCombat() {
    return Combat.builder()
        .teamFirst(createFighter("first"))
        .teamSecond(createFighter("second"))
        .events(new ArrayList<>())
        .actions(new ArrayList<>())
        .build();
  }

  private Fighter createFighter(String name) {
    return Fighter.builder()
        .name(name)
        .health(100)
        .healthMax(100)
        .build();
  }

}
