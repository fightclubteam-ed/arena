package ru.edubovit.fightclub.arena.web;

import ru.edubovit.fightclub.arena.domain.Action;
import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.service.CombatService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("combat")
@RequiredArgsConstructor
public class CombatController {

  private final CombatService combatService;

  @GetMapping("{id}")
  public ResponseEntity<Combat> getById(@PathVariable Long id) {
    return ResponseEntity.ok(combatService.getById(id));
  }

  @PostMapping
  public ResponseEntity<Long> create() {
    return ResponseEntity.status(HttpStatus.CREATED).body(combatService.create());
  }

  @PostMapping("{combatId}/action")
  public ResponseEntity<Void> postAction(@PathVariable Long combatId, @RequestBody Action action) {
    combatService.postAction(combatId, action);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

}
