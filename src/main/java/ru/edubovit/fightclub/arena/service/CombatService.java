package ru.edubovit.fightclub.arena.service;

import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.domain.Action;

public interface CombatService {

  Combat getById(Long id);
  Long create();
  void postAction(Long combatId, Action action);

}
