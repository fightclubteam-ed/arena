package ru.edubovit.fightclub.arena.service.impl;

import ru.edubovit.fightclub.arena.dao.CombatDAO;
import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.domain.Fighter;
import ru.edubovit.fightclub.arena.domain.Action;
import ru.edubovit.fightclub.arena.exception.NotFoundException;
import ru.edubovit.fightclub.arena.service.CombatService;
import ru.edubovit.fightclub.arena.service.ActionProcessor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class CombatServiceImpl implements CombatService {

  private final CombatDAO combatDAO;
  private final ActionProcessor actionProcessor;

  @Override
  public Combat getById(Long id) {
    var combat = combatDAO.getById(id);
    if (combat == null) {
      throw new NotFoundException(String.format("Combat %d not found", id));
    }
    return combat;
  }

  @Override
  public Long create() {
    var combat = Combat.builder()
        .teamFirst(createFighter("Даниэль"))
        .teamSecond(createFighter("Александр"))
        .actions(new ArrayList<>())
        .events(new ArrayList<>())
        .build();
    combatDAO.save(combat);
    return combat.getId();
  }

  @Override
  public void postAction(Long combatId, Action action) {
    var combat = combatDAO.getById(combatId);
    actionProcessor.validateAction(combat, action);
    actionProcessor.processAction(combat, action);
  }

  private Fighter createFighter(String name) {
    return Fighter.builder()
        .name(name)
        .health(100)
        .healthMax(100)
        .build();
  }

}
