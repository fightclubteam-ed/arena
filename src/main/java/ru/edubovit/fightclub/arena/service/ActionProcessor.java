package ru.edubovit.fightclub.arena.service;

import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.domain.Action;

public interface ActionProcessor {

  void validateAction(Combat combat, Action action);
  void processAction(Combat combat, Action action);

}
