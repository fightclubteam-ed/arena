package ru.edubovit.fightclub.arena.service.impl;

import ru.edubovit.fightclub.arena.domain.Action;
import ru.edubovit.fightclub.arena.domain.Combat;
import ru.edubovit.fightclub.arena.domain.Fighter;
import ru.edubovit.fightclub.arena.domain.TurnEvent;
import ru.edubovit.fightclub.arena.domain.enums.Target;
import ru.edubovit.fightclub.arena.exception.BadRequestException;
import ru.edubovit.fightclub.arena.exception.GoneException;
import ru.edubovit.fightclub.arena.exception.InvalidTurnException;
import ru.edubovit.fightclub.arena.exception.NotFoundException;
import ru.edubovit.fightclub.arena.exception.RepeatedTurnException;
import ru.edubovit.fightclub.arena.service.ActionProcessor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class ActionProcessorImpl implements ActionProcessor {

  private Random random = new Random();

  @Override
  public void validateAction(Combat combat, Action action) {
    if (combat == null) {
      throw new NotFoundException("Combat not found");
    }
    if (combat.getWinner() != null) {
      throw new GoneException(String.format("Combat %d ended", combat.getId()));
    }
    if (isRepeatedTurn(action, combat.getActions())) {
      throw new RepeatedTurnException();
    }
    checkTurnValidity(action);
  }

  @Override
  public void processAction(Combat combat, Action action) {
    combat.addAction(action);
    if (isReadyToFulfil(combat)) {
      var turn = combat.constructTurn();
      fulfilAgainst(turn.getTeamFirstAction(), turn.getTeamSecondAction(), combat);
      fulfilAgainst(turn.getTeamSecondAction(), turn.getTeamFirstAction(), combat);
      ensureWinner(combat);
    }
  }

  private void fulfilAgainst(Action offenderAction, Action defenderAction, Combat combat) {
    var offender = offenderAction.getOwner();
    var defender = defenderAction.getOwner();
    for (var target : offenderAction.getAttack()) {
      if (defenderAction.getProtect().contains(target)) {
        miss(offender, defender, combat);
      } else {
        hit(offender, defender, target, combat);
      }
    }
  }

  private void miss(Fighter offender, Fighter defender, Combat combat) {
    String message = String.format("%s ->[MISSED] %s [%d/%d]", offender.getName(), defender.getName(),
        (int) Math.ceil(defender.getHealth()), (int) Math.ceil(defender.getHealthMax()));
    log(combat.getId(), message);
    combat.addTurnEvent(new TurnEvent(message));
  }

  private void hit(Fighter offender, Fighter defender, Target target, Combat combat) {
    double rnd = random.nextDouble();
    double damage = 5 + 30 * rnd * rnd;
    double prevHealth = defender.getHealth();
    double newHealth = prevHealth - damage < 0 ? 0d : prevHealth - damage;
    String message = String.format("%s ->[%s] %s: -%d HP [%d/%d]",
        offender.getName(), target, defender.getName(), Math.round(damage),
        (int) Math.ceil(newHealth), (int) Math.ceil(defender.getHealthMax()));
    log(combat.getId(), message);
    defender.setHealth(newHealth);
    combat.addTurnEvent(new TurnEvent(message));
  }

  private boolean isReadyToFulfil(Combat combat) {
    return combat.getActions().size() % 2 == 0;
  }

  private void ensureWinner(Combat combat) {
    var first = combat.getTeamFirst();
    var second = combat.getTeamSecond();

    if (first.getHealth() <= 0d && second.getHealth() <= 0d) {
      combat.setWinner(Combat.Winner.TIE);
      String message = String.format("Tie! %s [%d/%d] : %s [%d/%d]",
          first.getName(), (int) Math.ceil(first.getHealth()), (int) Math.ceil(first.getHealthMax()),
          second.getName(), (int) Math.ceil(second.getHealth()), (int) Math.ceil(second.getHealthMax()));
      log(combat.getId(), message);
      combat.addTurnEvent(new TurnEvent(message));
    } else if (first.getHealth() <= 0d) {
      combat.setWinner(Combat.Winner.SECOND);
      String message = String.format("%s won! %s [%d/%d] : %s [%d/%d]", second.getName(),
          first.getName(), (int) Math.ceil(first.getHealth()), (int) Math.ceil(first.getHealthMax()),
          second.getName(), (int) Math.ceil(second.getHealth()), (int) Math.ceil(second.getHealthMax()));
      log(combat.getId(), message);
      combat.addTurnEvent(new TurnEvent(message));
    } else if (second.getHealth() <= 0d) {
      combat.setWinner(Combat.Winner.FIRST);
      String message = String.format("%s won! %s [%d/%d] : %s [%d/%d]", first.getName(),
          first.getName(), (int) Math.ceil(first.getHealth()), (int) Math.ceil(first.getHealthMax()),
          second.getName(), (int) Math.ceil(second.getHealth()), (int) Math.ceil(second.getHealthMax()));
      log(combat.getId(), message);
      combat.addTurnEvent(new TurnEvent(message));
    }
  }

  private boolean isRepeatedTurn(Action action, List<Action> actions) {
    return actions.size() % 2 == 1
        && action.getOwner().getName().equalsIgnoreCase(
        actions.get(actions.size() - 1).getOwner().getName());
  }

  private void checkTurnValidity(Action action) {
    var attack = action.getAttack();
    var protect = action.getProtect();
    if (attack == null || protect == null) {
      throw new BadRequestException("Attack and protect targets should be pointed");
    }
    if (attack.size() != 1) {
      throw new InvalidTurnException(String.format("%d targets to attack received, expected %d", attack.size(), 1));
    }
    if (protect.size() != 3) {
      throw new InvalidTurnException(String.format("%d targets to protect received, expected %d", protect.size(), 3));
    }
  }

  private void log(Long combatId, String message) {
    LOGGER.info("Combat {} :: {}", combatId, message);
  }

}
