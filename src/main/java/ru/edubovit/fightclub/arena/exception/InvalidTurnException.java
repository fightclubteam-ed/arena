package ru.edubovit.fightclub.arena.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTurnException extends RuntimeException {

  public InvalidTurnException(String message) {
    super("Invalid turn: " + message);
  }

}
