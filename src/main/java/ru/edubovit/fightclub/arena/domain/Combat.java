package ru.edubovit.fightclub.arena.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Combat {

  private Long id;
  private Fighter teamFirst;
  private Fighter teamSecond;
  private List<Action> actions;
  private List<TurnEvent> events;
  private Winner winner;

  public void addAction(Action action) {
    if (teamFirst.getName().equalsIgnoreCase(action.getOwner().getName())) {
      action.setOwner(teamFirst);
    } else {
      action.setOwner(teamSecond);
    }
    actions.add(action);
  }

  public Turn constructTurn() {
    var actionFirst = actions.get(actions.size() - 2);
    var actionSecond = actions.get(actions.size() - 1);
    if (actionFirst.getOwner().getName().equalsIgnoreCase(teamFirst.getName())) {
      return Turn.builder()
          .teamFirstAction(actionFirst)
          .teamSecondAction(actionSecond)
          .build();
    } else {
      return Turn.builder()
          .teamFirstAction(actionSecond)
          .teamSecondAction(actionFirst)
          .build();
    }
  }

  public void addTurnEvent(TurnEvent turnEvent) {
    events.add(turnEvent);
  }

  public enum Winner {
    FIRST, SECOND, TIE
  }

}
