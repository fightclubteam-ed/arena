package ru.edubovit.fightclub.arena.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Fighter {

  private String name;
  private double health;
  private double healthMax;

}
