package ru.edubovit.fightclub.arena.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Turn {

  private Action teamFirstAction;
  private Action teamSecondAction;

}
