package ru.edubovit.fightclub.arena.domain.enums;

public enum Target {
  HEAD, RIGHT_HAND, BODY, LEFT_HAND, LEGS
}
