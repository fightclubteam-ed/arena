package ru.edubovit.fightclub.arena.domain;

import ru.edubovit.fightclub.arena.domain.enums.Target;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class Action {

  private Fighter owner;
  private Set<Target> attack;
  private Set<Target> protect;

}
