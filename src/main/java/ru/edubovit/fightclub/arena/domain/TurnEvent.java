package ru.edubovit.fightclub.arena.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TurnEvent {

  private String message;

}
