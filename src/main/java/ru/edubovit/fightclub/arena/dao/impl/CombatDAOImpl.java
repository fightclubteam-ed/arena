package ru.edubovit.fightclub.arena.dao.impl;

import org.springframework.stereotype.Repository;
import ru.edubovit.fightclub.arena.dao.CombatDAO;
import ru.edubovit.fightclub.arena.domain.Combat;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CombatDAOImpl implements CombatDAO {

  private Long currentId = 1L;
  private Map<Long, Combat> combats = new HashMap<>();

  @Override
  public Combat getById(Long id) {
    return combats.get(id);
  }

  @Override
  public void save(Combat combat) {
    combats.put(currentId, combat);
    combat.setId(currentId++);
  }

}
