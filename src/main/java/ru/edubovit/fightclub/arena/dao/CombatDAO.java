package ru.edubovit.fightclub.arena.dao;

import ru.edubovit.fightclub.arena.domain.Combat;

public interface CombatDAO {

  Combat getById(Long id);
  void save(Combat combat);

}
